//array for missed letters
//array for word to guess
//array for blanks (dashes)

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        // write your code here
        Scanner input = new Scanner(System.in);
        int chances = 6;
        String[] words = {"hangman", "java", "keyboard", "juice", "lebron"};
        Random rand = new Random();
        guess(words[rand.nextInt(words.length-1)], chances);


        while(true)
        {
            System.out.println("Do you want to play again? (yes or no)");
            String response = input.nextLine().toLowerCase();
            if(response.contains("y"))
            {
                //guess(words, chances);
                guess(words[rand.nextInt(words.length-1)], chances);
            }
            else
                {
                break;
                }
        }
    }

    //method to pass life(masked) and word as argument
        private static void guess(String word, int chances) {
            char[] dashes = new char[word.length()]; //the blanks the user will see when guessing the word
        //String guesses = "";   //house state of correct guesses
        //String dashes = "";
            for(int i = 0; i < word.length(); i++ )
            {
                dashes[i] = '_';
            }

        //System.out.println(dashes);
                     for (int i = 0; i < dashes.length; i++) {
                     System.out.print(dashes[i] + " ");
        }
        //System.out.println(" " + dashes);
                    System.out.println();
                    System.out.println("Welcome to H A N G M A N");
                    System.out.println("Come on, pick a letter");

                    Scanner letter = new Scanner(System.in); // reads letter guess from user
                    ArrayList<Character> same = new ArrayList<Character>(); //creates arraylist object for letters that have been used
                    ArrayList<Character> miss = new ArrayList<Character>(); //creates arraylist for letters not in word

        while(chances>0) {
            fillHangman(miss.size());
            char x = letter.next().charAt(0); //reads letter & enter it at first index
            String temp = String.valueOf(x);
            //if(word.equals(temp)) {
            //  guesses += x;
            //}

            if (same.contains(x) || miss.contains(x)) {
                System.out.println("You picked that already. Pick a new letter");
                System.out.println(same);
            }
           // same.add(x);
            else {
                same.add(x);
                if (word.indexOf(x) > -1)  //comparison
                {
                    for (int y = 0; y < word.length(); y++) {      //check all indexes for
                        if (word.charAt(y) == x) {                //the character and replace _
                            //dashes[y]=x;                      //with the character  //char array
                            dashes[y] = x;                       // converts char to string // string array
                        }
                    }
                } else {
                    miss.add(x);
                    chances--;                          //chances decrease if character not in word
                }
            }

            //same.add(x);

            if (word.equals(new String(dashes)))     //checking if blanks equals word
            {
                //System.out.println(Arrays.toString(dashes));
                System.out.println(dashes);
                System.out.println("Congrats!! You guessed it");
                break;
            }

            for (int i = 0; i < dashes.length; i++) {
                System.out.print(dashes[i] + " ");
            }
            System.out.println();
            System.out.println("You have " + chances + " left.");
            System.out.println("Missed letters: " + miss);
            //System.out.println("Guess again");

            //System.out.println("Guess again");

            if (chances == 0) {
                System.out.println("Haha. You lose.");
            }
        }
    }

        public static void fillHangman(int n){
            switch (n){
                case 0:
                    System.out.println("""
                +----+
                     |
                     |
                     |
                    ===
                """);
                break;

                case 1:
                    System.out.println("""
                    +----+
                    O    |
                         |
                         |
                        ===
                    """);
                    break;

                case 2:
                    System.out.println("""
                    +----+
                    O    |
                    |    |
                         |
                        ===
                    """);
                    break;

                case 3:
                    System.out.println("""
                      +----+
                      O    |
                     \\|    |
                           |
                          ===
                    """);
                    break;

                case 4:
                    System.out.println("""
                    +----+
                    O    |
                   \\|/   |
                         |
                        ===
                    """);
                    break;

                case 5:
                    System.out.println("""
                    +----+
                    O    |
                   \\|/   |
                   /     |
                        ===
                    """);
                    break;

                case 6:
                    System.out.println("""
                    +----+
                    O    |
                   \\|/   |
                   / \\   |
                        ===
                    """);
                    //break;
            }
    }
}



